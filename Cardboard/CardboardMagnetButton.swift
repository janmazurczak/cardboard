//
//  CardboardMagnetButton.swift
//  Cardboard
//
//  Created by Jan Mazurczak on 31/01/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import Foundation
import CoreMotion

public protocol CardboardMagnetButtonDelegate: class {
    func actionCardboardButtonPushed()
}

public class CardboardMagnetButton {
    
    public init() { }
    public weak var delegate: CardboardMagnetButtonDelegate?
    
    private let magneticFieldExpectedDelta: Double = 180
    private let pushToPushMinimumTimeInterval: NSTimeInterval = 1
    public var updateInterval: NSTimeInterval = 0.1
    
    private var lastPushTimeInterval: NSTimeInterval = 0
    private var previousState: CMMagneticField?
    public func updateMagneticField(magneticField: CMMagneticField) {
        
        defer {
            previousState = magneticField
            lastPushTimeInterval = lastPushTimeInterval + updateInterval
        }
        guard let lastState = previousState else { return }
        
        let delta = fabs(magneticField.x - lastState.x) + fabs(magneticField.y - lastState.y) + fabs(magneticField.z - lastState.z)
        guard delta > magneticFieldExpectedDelta else { return }
        guard lastPushTimeInterval > pushToPushMinimumTimeInterval else { return }
        
        lastPushTimeInterval = 0
        delegate?.actionCardboardButtonPushed()
    }
    
    public func holdPushesDiscoveryForTimeInterval(timeInterval: NSTimeInterval) {
        lastPushTimeInterval = -timeInterval
    }
    
}
