//
//  CardboardViewController.swift
//  Cardboard
//
//  Created by Jan Mazurczak on 27/01/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import UIKit
import SceneKit
import CoreMotion

let Device4InchesOrSmaller = min(UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height) < 325
let Device5InchesOrSmaller = min(UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height) < 380

public class CardboardViewController: UIViewController, CardboardMagnetButtonDelegate {
    
    //MARK: Motion tracking
    
    let motionManager = CMMotionManager()
    
    //MARK: Inits
    
    public init(scene: SCNScene) {
        self.scene = scene
        super.init(nibName: nil, bundle: nil)
    }
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        scene = SCNScene()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required public init?(coder aDecoder: NSCoder) {
        scene = SCNScene()
        super.init(coder: aDecoder)
    }
    
    public let scene: SCNScene
    public let cardboardNode = CardboardNode()
    
    //MARK: View handling
    
    weak var leftView: SCNView?
    weak var rightView: SCNView?
    
    public override func loadView() {
        let leftView = SCNView(frame: CGRectZero, options: [SCNPreferredRenderingAPIKey: SCNRenderingAPI.OpenGLES2.rawValue])
        let rightView = SCNView(frame: CGRectZero, options: [SCNPreferredRenderingAPIKey: SCNRenderingAPI.OpenGLES2.rawValue])
        
        view = UIView()
        for subview in [leftView, rightView] {
            subview.backgroundColor = .blackColor()
            subview.scene = scene
            subview.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(subview)
        }
        
        NSLayoutConstraint.activateConstraints(
            NSLayoutConstraint.constraintsWithVisualFormat("H:|[left][right]|", options: [], metrics: nil, views: ["left":leftView, "right":rightView]) +
            NSLayoutConstraint.constraintsWithVisualFormat("V:|[left]|", options: [], metrics: nil, views: ["left":leftView]) +
            NSLayoutConstraint.constraintsWithVisualFormat("V:|[right]|", options: [], metrics: nil, views: ["right":rightView]) +
            [NSLayoutConstraint(item: leftView, attribute: .Width, relatedBy: .Equal, toItem: rightView, attribute: .Width, multiplier: 1.0, constant: 0.0)]
        )
        
        scene.rootNode.addChildNode(cardboardNode)
        leftView.pointOfView = cardboardNode.leftCameraNode
        rightView.pointOfView = cardboardNode.rightCameraNode
        
        self.leftView = leftView
        self.rightView = rightView
        
        motionManager.deviceMotionUpdateInterval = 1.0 / (Device4InchesOrSmaller ? 30.0 : 60.0)
        motionManager.startDeviceMotionUpdatesUsingReferenceFrame(CMAttitudeReferenceFrame.XArbitraryZVertical, toQueue: NSOperationQueue.mainQueue()) {
            [weak self] (motion, error) -> Void in
            self?.cardboardNode.motionAttitude = motion?.attitude
        }
        
        cardboardButton = CardboardMagnetButton()
        cardboardButton!.delegate = self
        motionManager.magnetometerUpdateInterval = cardboardButton!.updateInterval
        motionManager.startMagnetometerUpdatesToQueue(NSOperationQueue.mainQueue()) {
            [weak self] (magnetometerData, error) -> Void in
            guard let magneticField = magnetometerData?.magneticField else { return }
            self?.cardboardButton?.updateMagneticField(magneticField)
        }
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:"actionTap:"))
    }
    
    func unloadView() {
        view = nil
        motionManager.stopDeviceMotionUpdates()
        motionManager.stopMagnetometerUpdates()
        cardboardButton = nil
        NSLog("unloaded view")
    }
    
    //MARK: Magnet button
    
    var cardboardButton: CardboardMagnetButton?
    public func actionCardboardButtonPushed() {
        
    }
    
    public var increaseBrightnessOnPresentation: Bool = true
    private var originalBrightness: CGFloat?
    
    public override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if increaseBrightnessOnPresentation {
            originalBrightness = UIScreen.mainScreen().brightness
            UIScreen.mainScreen().brightness = 1.0
        }
    }
    
    public override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if let originalBrightness = originalBrightness {
            UIScreen.mainScreen().brightness = originalBrightness
        }
    }
    
    //MARK: Dismissing
    
    public var dismissOnTap: Bool = true
    
    func actionTap(tap: UITapGestureRecognizer) {
        if dismissOnTap {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    //MARK: Standards
    
    override public func shouldAutorotate() -> Bool {
        return true
    }
    
    override public func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override public func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .LandscapeRight
    }
    
    override public func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return .LandscapeRight
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if isViewLoaded() && view.window == nil {
            unloadView()
        }
    }
    
}
