//
//  CardboardNode.swift
//  Cardboard
//
//  Created by Jan Mazurczak on 29/01/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import SceneKit
import CoreMotion

public class CardboardNode: SCNNode {
    
    let leftCamera = SCNCamera()
    let rightCamera = SCNCamera()
    let leftCameraNode = SCNNode()
    let rightCameraNode = SCNNode()
    let rotatingNode = SCNNode()
    
    public var eyesDistance: Float = 0.065 { didSet { resetEyesPositionAndFocus() } }
    public var focusDistance: Float = 10 { didSet { resetEyesPositionAndFocus() } }
    
    func resetEyesPositionAndFocus() {
        leftCameraNode.position.x = eyesDistance * -0.5
        rightCameraNode.position.x = eyesDistance * 0.5
        
        let angle = atan((0.5 * eyesDistance) / focusDistance)
        leftCameraNode.rotation = SCNVector4(0, -1, 0, angle)
        rightCameraNode.rotation = SCNVector4(0, 1, 0, angle)
    }
    
    public override init() {
        super.init()
        setupOnce()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    func setupOnce() {
        for camera in [leftCamera, rightCamera] {
            let cameraAngle = Device4InchesOrSmaller ? 45 : Device5InchesOrSmaller ? 60.0 : 70.0
            camera.xFov = cameraAngle
            camera.yFov = cameraAngle
            camera.zNear = 0.1
            camera.zFar = 1100
        }
        leftCameraNode.camera = leftCamera
        rightCameraNode.camera = rightCamera
        rotatingNode.addChildNode(leftCameraNode)
        rotatingNode.addChildNode(rightCameraNode)
        addChildNode(rotatingNode)
        resetEyesPositionAndFocus()
    }
    
    public var motionAttitude: CMAttitude? {
        didSet {
            var matrix = SCNMatrix4Identity
            defer { rotatingNode.transform = matrix }
            
            guard let attitude = motionAttitude else { return }
            let deviceMatrix = SCNMatrix4(CMRotationMatrix: attitude.rotationMatrix)
            
            matrix = SCNMatrix4Rotate(matrix, Float(M_PI_2), 0, 0, -1)
            matrix = SCNMatrix4Mult(matrix, deviceMatrix)
            matrix = SCNMatrix4Rotate(matrix, Float(M_PI_2), -1, 0, 0)
            matrix = SCNMatrix4Rotate(matrix, Float(M_PI_2), 0, 1, 0)
        }
    }
    
}

extension SCNMatrix4 {
    init(CMRotationMatrix m: CMRotationMatrix) {
        self.init(
            m11: Float(m.m11), m12: Float(m.m12), m13: Float(m.m13), m14: 0.0,
            m21: Float(m.m21), m22: Float(m.m22), m23: Float(m.m23), m24: 0.0,
            m31: Float(m.m31), m32: Float(m.m32), m33: Float(m.m33), m34: 0.0,
            m41: 0.0, m42: 0.0, m43: 0.0, m44: 1.0)
    }
}
