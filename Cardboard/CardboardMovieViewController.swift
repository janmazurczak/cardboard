//
//  CardboardMovieViewController.swift
//  Cardboard
//
//  Created by Jan Mazurczak on 27/01/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import UIKit
import AVFoundation
import SceneKit
import SpriteKit

public class CardboardMovieViewController: CardboardViewController {
    
    let movieSphere: SCNNode = {
        let material = SCNMaterial()
        material.doubleSided = true
        material.diffuse.mipFilter = .Linear
        material.lightingModelName = SCNLightingModelConstant
        
        let geometry = SCNSphere(radius: 10)
        geometry.firstMaterial = material
        
        let node = SCNNode(geometry: geometry)
        node.rotation = SCNVector4(1, 0, 0, M_PI)
        
        return node
    }()
    
    public init(movieURL: NSURL) {
        player = AVPlayer(URL: movieURL)
        super.init(nibName: nil, bundle: nil)
        scene.rootNode.addChildNode(movieSphere)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func play() {
        playerNode?.play()
        leftView?.play(nil)
        rightView?.play(nil)
        UIApplication.sharedApplication().idleTimerDisabled = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerDidStop:", name: AVPlayerItemDidPlayToEndTimeNotification, object: player.currentItem)
    }
    
    public func pause() {
        playerNode?.pause()
        leftView?.pause(nil)
        rightView?.pause(nil)
        UIApplication.sharedApplication().idleTimerDisabled = false
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: player.currentItem)
    }
    
    /// Override this method to perform custom actions after playing movie is finished. Default this method does nothing.
    public func playerDidStop(notification: NSNotification) {
        
    }
    
    public var playing: Bool {
        return leftView?.playing ?? false
    }
    
    public let player: AVPlayer
    var skScene: SKScene?
    var playerNode: SKVideoNode?
    
    private var videoSize: CGSize {
        if Device4InchesOrSmaller {
            return CGSize(width: 1024, height: 1024)
        } else {
            return CGSize(width: 2048, height: 2048)
        }
    }
    
    public override func actionCardboardButtonPushed() {
        if playing {
            pause()
        } else {
            play()
        }
    }
    
    public override func dismissViewControllerAnimated(flag: Bool, completion: (() -> Void)?) {
        pause()
        super.dismissViewControllerAnimated(flag, completion: completion)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        playerNode = SKVideoNode(AVPlayer: player)
        playerNode?.size = videoSize
        playerNode?.position = CGPoint(x: 0.5 * videoSize.width, y: 0.5 * videoSize.height)
        skScene = SKScene(size: videoSize)
        skScene?.addChild(playerNode!)
        
        movieSphere.geometry?.firstMaterial?.diffuse.contents = skScene
        cardboardButton?.holdPushesDiscoveryForTimeInterval(3)
        
        pause()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func unloadView() {
        super.unloadView()
        pause()
        playerNode = nil
        skScene = nil
        movieSphere.geometry?.firstMaterial?.diffuse.contents = nil
    }
    
}
